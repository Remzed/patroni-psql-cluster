# Postgresql High Availability Cluster


* Patroni : Patroni is a template for you to create your own customized, high-availability solution using Python and - for maximum accessibility - a distributed configuration store like ZooKeeper, etcd, Consul or Kubernetes

* Etcd : etcd is a distributed reliable key-value store for the most critical data of a distributed system, written in Go and uses the Raft consensus algorithm to manage a highly-available replicated log.

* Postgresql-9.6 : PostgreSQL open source relational database management system (DBMS) developed by a worldwide team of volunteers

* HAProxy : HAProxy is a free, very fast and reliable solution offering high availability, load balancing, and proxying for TCP and HTTP-based applications.


## Setup 

3 nodes run individualy :

- 1 etcd node
- 1 patroni api
- 1 postgresql node (master/standby)
- 1 haproxy 


## Workflow

> Example: INSERT sql request

- Haproxy intercept postgresql request (127.0.0.1:5432)
- Check each patroni api to select who is master
- Send the request to psql-master
- Psql-master execute the request
- Postgresql replicate the request to the standby servers
- Inform etcd cluster that psql-master still master

![Pg-cluster-concept](pg-cluster-concept.png)
