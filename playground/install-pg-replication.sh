#!/bin/bash

# Postgresql Streaming Replication dirty way

INTERFACE=$(netstat -rn|head -n3|grep 0.0.0.0|awk {'print $8'})
IP=$(ifconfig "${INTERFACE}" | grep "inet addr" | cut -d ':' -f 2 | cut -d ' ' -f 1)
IP_MASTER='10.20.210.201'
IP_SLAVE='10.20.210.202'

# Installation 
sudo add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -sc)-pgdg main"
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -
sudo apt-get update
sudo apt-get install postgresql-9.6

sudo -u postgres psql -c "CREATE USER replicator REPLICATION LOGIN ENCRYPTED PASSWORD 'replicator';"
sudo -u postgres psql -c "CREATE USER replication REPLICATION LOGIN ENCRYPTED PASSWORD 'magency2016-replication';"
sudo -u postgres createdb magency
sudo -u postgres psql -c "create user magency with encrypted password 'magency2012';"
sudo -u postgres psql -c "grant all privileges to magency;"

systemctl stop postgresql.service

# Configuration
echo "
listen_addresses = '"${IP}"'
# To enable read-only queries on a standby server, wal_level must be set to
# "hot_standby". But you can choose "archive" if you never connect to the
# server in standby mode.
wal_level = hot_standby
# Set the maximum number of concurrent connections from the standby servers.
max_wal_senders = 5
wal_keep_segments = 8
"  >> /etc/postgresql/9.6/main/postgresql.conf

echo "
# The standby server must connect with a user that has replication privileges.
# TYPE  DATABASE        USER            ADDRESS                 METHOD
host  replication     replicator     ${IP_MASTER}/32            md5
host  replication     replicator     ${IP_SLAVE}/32             md5
host  all							magency 			 10.20.210.0/24							md5
"  >> /etc/postgresql/9.6/main/pg_hba.conf


echo "Need to clean old cluster data with : sudo -u postgres rm -rf /var/lib/postgresql/9.6/main" 

echo "Starting base backup as replicator"
sudo -u postgres pg_basebackup -h ${IP_MASTER} -D /var/lib/postgresql/9.6/main -U replicator -v -P


echo Writing recovery.conf file
sudo -u postgres bash -c "cat > /var/lib/postgresql/9.6/main/recovery.conf <<- _EOF1_
  standby_mode = 'on'
  primary_conninfo = 'host=${IP_MASTER} port=5432 user=replicator password=replicator '
  trigger_file = '/tmp/postgresql.trigger'
_EOF1_
"

systemctl start postgresql.service

sudo -u postgres psql -x -c "select * from pg_stat_replication;"
